﻿//Sample-ConvexHull
//ZIYI (joslimzy@gmail.com)
//Function: generate points randomly, and use convex hull to cover up all the points
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sample_ConvexHull
{
    public partial class Form1 : Form
    {

        List<Point> points;
        List<Point> points_convex_hull;
        Point[] convex_hull;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //button click
        private void button_random_Click(object sender, EventArgs e)
        {
            resetPoints();
            findConvexHull();
        }

        private void pictureBox_graph_Paint(object sender, PaintEventArgs e)
        {

            Graphics g = e.Graphics;
            g.Clear(this.BackColor);
            drawPoints(g, points);
            drawConvexHull(g ,convex_hull);
        }

        //Draw Points
        private void drawPoints(Graphics g, List<Point> points)
        {
            //Draw points
            if (points == null)
                return;

            Pen pen = new Pen(Color.Blue);
            int circle_size = 2;
            for (int i = 0; i < points.Count; i++)
            {
                g.DrawEllipse(pen, points[i].X, points[i].Y, circle_size, circle_size);
            }

        }

        //Draw convex hull
        private void drawConvexHull(Graphics g, Point[] convex_hull)
        {
            if (convex_hull == null)
                return;
            g.DrawPolygon(Pens.Red, convex_hull);
        }


        //random points
        private void resetPoints()
        {
            int x, y;
            int margin = 20;
            Random r = new Random(); //random object

            points = new List<Point>(); //re initialize List
            int num_points = int.Parse(textBox_numpoint.Text);
            for (int i = 0; i < num_points; i++)
            {
                x = r.Next(margin, pictureBox_graph.Width- margin);
                y = r.Next(margin, pictureBox_graph.Height- margin);
                points.Add(new Point(x,y));
            }

            convex_hull = null;
            pictureBox_graph.Refresh();

        }

        //find convex hull
        private void findConvexHull()
        {
            points_convex_hull = Geometry.MakeConvexHull(points); //points to convex hull
            convex_hull = new Point[points_convex_hull.Count];
            points_convex_hull.CopyTo(convex_hull); //convert list to array
            pictureBox_graph.Refresh();
        }
    }
}
