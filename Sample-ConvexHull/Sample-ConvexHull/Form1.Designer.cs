﻿namespace Sample_ConvexHull
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_reset = new System.Windows.Forms.Button();
            this.button_find = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox_numpoint = new System.Windows.Forms.TextBox();
            this.pictureBox_graph = new System.Windows.Forms.PictureBox();
            this.button_random = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_graph)).BeginInit();
            this.SuspendLayout();
            // 
            // button_reset
            // 
            this.button_reset.Location = new System.Drawing.Point(406, 12);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(75, 23);
            this.button_reset.TabIndex = 0;
            this.button_reset.Text = "Reset points";
            this.button_reset.UseVisualStyleBackColor = true;
            this.button_reset.Visible = false;
            // 
            // button_find
            // 
            this.button_find.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_find.Location = new System.Drawing.Point(487, 12);
            this.button_find.Name = "button_find";
            this.button_find.Size = new System.Drawing.Size(101, 23);
            this.button_find.TabIndex = 1;
            this.button_find.Text = "Find Convex Hull";
            this.button_find.UseVisualStyleBackColor = true;
            this.button_find.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_random);
            this.panel1.Controls.Add(this.textBox_numpoint);
            this.panel1.Controls.Add(this.button_reset);
            this.panel1.Controls.Add(this.button_find);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(600, 49);
            this.panel1.TabIndex = 2;
            // 
            // textBox_numpoint
            // 
            this.textBox_numpoint.Location = new System.Drawing.Point(13, 12);
            this.textBox_numpoint.Name = "textBox_numpoint";
            this.textBox_numpoint.Size = new System.Drawing.Size(59, 22);
            this.textBox_numpoint.TabIndex = 2;
            this.textBox_numpoint.Text = "100";
            // 
            // pictureBox_graph
            // 
            this.pictureBox_graph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_graph.Location = new System.Drawing.Point(0, 49);
            this.pictureBox_graph.Name = "pictureBox_graph";
            this.pictureBox_graph.Size = new System.Drawing.Size(600, 513);
            this.pictureBox_graph.TabIndex = 3;
            this.pictureBox_graph.TabStop = false;
            this.pictureBox_graph.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_graph_Paint);
            // 
            // button_random
            // 
            this.button_random.Location = new System.Drawing.Point(78, 11);
            this.button_random.Name = "button_random";
            this.button_random.Size = new System.Drawing.Size(52, 23);
            this.button_random.TabIndex = 3;
            this.button_random.Text = "random";
            this.button_random.UseVisualStyleBackColor = true;
            this.button_random.Click += new System.EventHandler(this.button_random_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 562);
            this.Controls.Add(this.pictureBox_graph);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Convex Hull Sample";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_graph)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_reset;
        private System.Windows.Forms.Button button_find;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox_numpoint;
        private System.Windows.Forms.PictureBox pictureBox_graph;
        private System.Windows.Forms.Button button_random;
    }
}

